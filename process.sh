log() {
    echo $1 1>&2;
}

# Get values from a .ini file
function iniget() {
    if [[ $# -lt 2 || ! -f $1 ]]; then
        echo "usage: iniget <file> [--list|<section> [key]]"
        return 1
    fi
    local inifile=$1
    
    if [ "$2" == "--list" ]; then
        for section in $(cat $inifile | grep "^\\s*\[" | sed -e "s#\[##g" | sed -e "s#\]##g"); do
            echo $section
        done
        return 0
    fi
    
    local section=$2
    local key
    [ $# -eq 3 ] && key=$3
    
    # This awk line turns ini sections => [section-name]key=value
    local lines=$(awk '/\[/{prefix=$0; next} $1{print prefix $0}' $inifile)
    lines=$(echo "$lines" | sed -e 's/[[:blank:]]*=[[:blank:]]*/=/g')
    while read -r line ; do
        if [[ "$line" = \[$section\]* ]]; then
            local keyval=$(echo "$line" | sed -e "s/^\[$section\]//")
            if [[ -z "$key" ]]; then
                echo $keyval
            else          
                if [[ "$keyval" = $key=* ]]; then
                    echo $(echo $keyval | sed -e "s/^$key=//")
                fi
            fi
        fi
    done <<<"$lines"
}

get_compression() {
  if [[ "$1" == "gzip" ]] || [[ -z "$1" ]]; then
    echo "gzip"
  elif [[ "$compression" == "zstd" ]]; then
    echo "zstd"
  fi
}
get_compression_ext() {
  if [[ "$1" == "gzip" ]] || [[ -z "$1" ]]; then
    echo "gz"
  elif [[ "$compression" == "zstd" ]]; then
    echo "zst"
  fi
}


create_instream() {
    job_config=$1
    driver="$(iniget "$job_config" Source driver)"
    if [[ "$driver" == "ssh-tar" ]]; then
        identity_file="$(iniget "$job_config" Source identity-file)"
        host="$(iniget "$job_config" Source host)"
        username="$(iniget "$job_config" Source username)"
        directory="$(iniget "$job_config" Source directory)"
        tar_options="$(iniget "$job_config" Source tar_options)"
        compression="$(iniget "$job_config" Source compression)"
        command="$(iniget "$job_config" Source command)"
        log "Using command: $command"
        if [[ -z "$command" ]]; then
          command="tar -c $tar_options -f - $directory"
        fi
        compression_command="$(get_compression "$compression")"

        cp $identity_file /tmp/id
        chmod 600 /tmp/id
        # chown $USER:$USER /tmp/id
        log "Opening source stream"
        ssh -o "StrictHostKeyChecking=no" -i /tmp/id $username@$host "$command | $compression_command"
        log "Source stream closed"
    elif [[ "$driver" == "mysql" ]]; then
        address="$(iniget "$job_config" Source address)"
        username="$(iniget "$job_config" Source username)"
        password="$(iniget "$job_config" Source password)"
        compression="$(iniget "$job_config" Source compression)"
        compression_command="$(get_compression "$compression")"
        database="$(iniget "$job_config" Source database)"

        if [[ -z "$database" ]]; then
          database="--all-databases"
        fi

        host="$(echo "$address" | cut -d':' -f1)"
        port="$(echo "$address" | cut -d':' -f2)"

        log "Opening source stream"
        mysqldump --host="$host" --user="$username" --port="$port" --password="$password" $database | $compression_command
        log "Source stream closed"
    elif [[ "$driver" == "postgresql" ]]; then
        address="$(iniget "$job_config" Source address)"
        username="$(iniget "$job_config" Source username)"
        password="$(iniget "$job_config" Source password)"
        compression="$(iniget "$job_config" Source compression)"
        database="$(iniget "$job_config" Source database)"
        compression_command="$(get_compression "$compression")"

        host="$(echo "$address" | cut -d':' -f1)"
        port="$(echo "$address" | cut -d':' -f2)"

        log "Opening source stream"
        PGPASSWORD="$password" pg_dump --format=tar --host="$host" --username="$username" --port="$port" --dbname $database | $compression_command
        log "Source stream closed"
    elif [[ "$driver" == "seaweedfs" ]]; then
        filer="$(iniget "$job_config" Source filer)"
        path="$(iniget "$job_config" Source path)"
        path="${path:-/}"
        compression="$(iniget "$job_config" Source compression)"
        compression_command="$(get_compression "$compression")"
        tar_options="$(iniget "$job_config" Source tar_options)"

        log "Preparing SeaweedFS mount"
        rm -rf /mnt
        mkdir -p /mnt
        weed mount -filer="$filer" -filer.dir="$path" -cacheCapacityMB=10 -dir="/mnt" -readOnly

        log "Opening source stream"
        tar -c $tar_options -f - /mnt | $compression_command
        log "Source stream closed"
    elif [[ "$driver" == "s3" ]]; then
        endpoint="$(iniget "$job_config" Source endpoint)"
        access_key_id="$(iniget "$job_config" Source access_key_id)"
        secret_access_key="$(iniget "$job_config" Source secret_access_key)"
        region="$(iniget "$job_config" Source region)"
        ca_cert="$(iniget "$job_config" Source ca_cert)"
        bucket="$(iniget "$job_config" Source bucket)"
        path="$(iniget "$job_config" Source path)"
        path="${path:-/}"
        compression="$(iniget "$job_config" Source compression)"
        compression_command="$(get_compression "$compression")"
        tar_options="$(iniget "$job_config" Source tar_options)"

        echo "[s3]" >  /rclone.conf
        echo "type = s3" >>  /rclone.conf
        echo "provider = Other" >>  /rclone.conf
        echo "access_key_id = $access_key_id" >> /rclone.conf
        echo "secret_access_key = $secret_access_key" >>  /rclone.conf
        echo "region = $region" >>  /rclone.conf
        echo "endpoint = $endpoint" >>  /rclone.conf

        if [[ ! -z "$ca_cert" ]]; then
          ca_cert="--ca-cert $ca_cert"
        fi

        log "Preparing mount"
        rm -rf /mnt
        mkdir -p /mnt
        rclone mount $ca_cert --config /rclone.conf "s3:$bucket" /mnt &
        rclone_pid="$!"
        sleep 2

        log "Opening source stream"
        tar -c $tar_options -f - /mnt | $compression_command
        log "Source stream closed"
        kill $rclone_pid
    fi
}

generate_filename() {
    job_config=$1
    driver="$(iniget "$job_config" Source driver)"
    # datestamp="$(date +%F_%H:%M:%S)"

    if [[ "$driver" == "ssh-tar" ]] || [[ "$driver" == "seaweedfs" ]] || [[ "$driver" == "s3" ]]; then
        compression_ext="$(get_compression_ext "$compression")"
        job_name="$(iniget $job_config Job id)"
        echo "backup_${job_name}.tar.${compression_ext}.asc"
    elif [[ "$driver" == "mysql" ]]; then
        compression_ext="$(get_compression_ext "$compression")"
        job_name="$(iniget $job_config Job id)"
        echo "backup_${job_name}.sql.${compression_ext}.asc"
    elif [[ "$driver" == "postgresql" ]]; then
        compression_ext="$(get_compression_ext "$compression")"
        job_name="$(iniget $job_config Job id)"
        echo "backup_${job_name}.tar.${compression_ext}.asc"
    fi
}

encrypt() {
    job_config=$1
    job_password="$(iniget $job_config Job password)"

    cat - | openssl enc -aes-256-cbc -pbkdf2 -pass "pass:$job_password"
}

create_outstream() {
    job_config=$1
    filename=$2
    driver="$(iniget "$job_config" Destination driver)"

    if [[ "$driver" == "rsync.net" ]]; then
        account="$(iniget "$job_config" Destination account)"
        folder="$(iniget "$job_config" Destination folder)"
        identity_file="$(iniget "$job_config" Destination identity-file)"
        ssh_connection_string="$account@${account}.rsync.net"

        cp $identity_file /tmp/destid
        chmod 600 /tmp/destid
        # chown $USER:$USER /tmp/destid
        log "Ensuring destination folder exists"
        # -n = ignore stdin and stop SLORPING it
        ssh -n -o "StrictHostKeyChecking=no" -i /tmp/destid $ssh_connection_string "mkdir -p $folder"
        log "Opening destination stream"
        ssh -o "StrictHostKeyChecking=no" -i /tmp/destid $ssh_connection_string "dd of=$folder/$filename"
        log "Destination stream finished."
    fi
}


JOB_DIRECTORY="${JOB_DIRECTORY:-/etc/jobs}"

for job_config in $JOB_DIRECTORY/*; do
    log "Processing job from $job_config"
    filename="$(generate_filename "$job_config")"
    create_instream "$job_config" | encrypt "$job_config" | create_outstream "$job_config" "$filename"
    log "Job finished"
done