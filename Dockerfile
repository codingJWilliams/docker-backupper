FROM alpine

RUN apk add --no-cache mariadb-client zstd bash openssh-client openssl wget postgresql rclone fuse3
RUN wget https://github.com/seaweedfs/seaweedfs/releases/download/3.61/linux_amd64_full.tar.gz && tar xvzf linux_amd64_full.tar.gz && rm -rf linux_amd64_full.tar.gz && mv weed /bin
COPY ./process.sh .

CMD ["/bin/bash", "process.sh"]